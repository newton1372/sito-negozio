import '../CSS/Cart.css';
import PaymentModule from './PaymentModule'
import PayPalProperties from '../Properties/PayPalProperties'
import React from 'react';
import {PayPalButton} from 'react-paypal-button-v2';



class Cart extends React.Component {
  constructor(props){
    super(props);  
    this.calculateTotal=this.calculateTotal.bind(this);
    this.changePaymentSelected=this.changePaymentSelected.bind(this);
    this.toggleSumUpCard=this.toggleSumUpCard.bind(this);
    this.setInfoNotification=this.setInfoNotification.bind(this);

    this.state={
      carrello:[],
      paymentSelected:"PayPal",
      sumUpVisible:false,
      infoNotification:null
    }

  }

  

  setInfoNotification(obj){
    var self=this;
    var newState = Object.assign(self.state);
    newState.infoNotification =obj;
    this.setState(newState);
  }


  calculateTotal(){
    var price=0;
    this.state.carrello.forEach(function(item){
      price += parseFloat(item.prezzo);
    })
    return price.toFixed(2);
  }




  componentWillReceiveProps(newProps){
    var self=this;
    var newState = Object.assign(self.state);
    newState.carrello = newProps.carrello;
    this.setState(newState);
  }


 componentDidMount(){
  window.SumUpCard.mount({
    checkoutId: '2ceffb63-cbbe-4227-87cf-0409dd191a98',
    onResponse: function(type, body) {
        console.log('Type', type);
        console.log('Body', body);
    }
});
 }



 changePaymentSelected(str){
  var self=this;
  var newState = Object.assign(self.state);
  newState.paymentSelected = str;
  this.setState(newState);
 }


 toggleSumUpCard(){
  var self=this;
  var newState = Object.assign(self.state);
  newState.sumUpVisible = !newState.sumUpVisible;
  this.setState(newState);
 }


 
  render(){
    var self=this;
    var total =this.calculateTotal();
    var paymentButton =<div/>
    var sumupClassVisibility= self.state.sumUpVisible? "sumup-card-visible" : "sumup-card-invisible";
    

    switch(self.state.paymentSelected){
        case "PayPal": paymentButton=
                                <div className="button-paypal-container"> 
                                  <PayPalButton
                                      
                                      amount={total}
                                      // shippingPreference="NO_SHIPPING" // default is "GET_FROM_FILE"
                                      createOrder={(data, actions) => {
                                        return actions.order.create({
                                          purchase_units: [{
                                            amount: {
                                              currency_code: "EUR",
                                              value:  total
                                            }
                                          }],
                                          // application_context: {
                                          //   shipping_preference: "NO_SHIPPING" // default is "GET_FROM_FILE"
                                          // }
                                        });
                                      }}
                                      onSuccess={(details, data) => {
                                        alert("Pagamento effettuato correttamente!");

                                        // OPTIONAL: Call your server to save the transaction
                                        return fetch("/paypal-transaction-complete", {
                                          method: "post",
                                          body: JSON.stringify({
                                            orderID: data.orderID
                                          })
                                        });
                                      }}

                                      onError = {(details,data) =>{
                                        alert("Transazione fallita. Il pagamento non è stato effettuato");
                                        console.log("DETAILS",details);
                                      }}
                                      
                                      //options={{clientId:"AYvefKxfVqGbAPLGYFu7A7Q9iut-CFF7hgvjvJz8I_x-VBL-uPfZw7AidCLM52b-nsuPqoVPSrkgyeOs", currency:"EUR"}}
                                      options={{clientId: PayPalProperties.clientId, currency:"EUR"}}   
                                      //options={{clientId:"sb", currency:"EUR"}}

                                      style={{layout:"horizontal",height:40,color:"gold"}}                      
                                  />
                            </div>;
                            break;
            case "SumUp":   paymentButton =  <button className="sumup-button-payment-cart" value="SumUp" onClick={this.toggleSumUpCard}>Sum Up</button> 
                                          
                            break;
            default: paymentButton = <div/>

        }
                            




    return ( 
              <div className="cart-content">
                    <div className="titolo-pagamento">Carrello</div>
                     
                    <div className="cart-product-list">
                      {
                        self.state.carrello.map(function(item){
                          return(
                            <div className="product-div-cart"> 
                                <div className="nome-cart">{item.nome}</div>
                                <div className="codice-cart">{item.codice}</div>
                                <div className="prezzo-cart">{item.prezzo}€</div>
                            </div>              
                              )
                              })
                              }

                            </div>{/*  
                          */}<div className="total-form-payment">
                                  <PaymentModule changePaymentSelected={self.changePaymentSelected} setInfoNotification={self.setInfoNotification} />
                            </div>
                            <div className="total-price-div">
                                <div className="total-label">
                                  Totale:
                                </div>{/* 
                            */}<div className ="price">
                                {total}
                                <div className = "euro-symbol-cart">€</div>
                              </div>
                            </div>
                            {paymentButton}
                           <div id="sumup-card" className = {sumupClassVisibility} ></div>
                                        
          
              </div>
  );
}
}
export default Cart;

