import '../CSS/Contatti.css';
import React from 'react';
import ContattiProperties from "../Properties/ContattiProperties";
class ContattiTooltip extends React.Component {

  constructor(props){
    super(props);
    this.sendMail = this.sendMail.bind(this);
  }



  sendMail(){


  }


  render(){
    
        

  return (
              <div className="contatti-tooltip-container" onMouseOver={this.props.openParentMethod} onMouseLeave={this.props.closeParentMethod}>
                <div className="logo-contatti"></div>

                <div className="facebook-contatti">      
                    <div className="facebook-logo"/>{/* 
                     */}<div className="label-facebook">{ContattiProperties.facebook}</div>
                </div>
                <div className="whatsapp-contatti">
                  <div className="whatsapp-logo"/>{/* 
                     */}<div className="label-whatsapp">{ContattiProperties.whatsapp}</div>
                </div>
                <div className="email-contatti">
                  <div className="email-logo"/>{/* 
                     */}<div className="label-email">
                       <a href={"mailto:"+ContattiProperties.email}>{ContattiProperties.email}</a>
                  </div>
                </div>
                <div className="cell-contatti">
                  <div className="cell-logo"/>{/* 
                     */}<div className="label-cell"><a>{ContattiProperties.cell}</a></div>
                </div>
            </div>
  );
}
}
export default ContattiTooltip;

