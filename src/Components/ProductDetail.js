import '../CSS/ProductDetail.css';
import React from 'react';

class ProductDetail extends React.Component {
  constructor(props){
    super(props);  
 
    this.clickCart=this.clickCart.bind(this);


    this.state={

    }
  }



  
 

  clickCart(){
    var self=this;
    if(!self.props.productInCart){
      this.props.addToCart(this.props.productProperties);
    }
    else{
      this.props.removeToCart(this.props.productProperties);
    }
  }




  render(){
    
    var buttonLabel = this.props.productInCart ? "Togli dal carrello" : "Aggiungi al carrello";
    

  return (
              <div className="father-tooltip-component" onMouseLeave={this.hidePayment}>
                
                <div className="main-div-tooltip">
                    <div className ="header-detail">
                      <div className="nome-detail">
                      {this.props.productProperties.nome}
                      </div>
                    </div>
                    <div className ="body-detail">
                      <div className="column-left-detail">
                        <div className="image-detail-2"  style={{backgroundImage: "url("+this.props.productProperties.immagine2+")"}}/>
                        <div className="image-detail-3"  style={{backgroundImage: "url("+this.props.productProperties.immagine3+")"}}/>
                      </div>
                      <div className="column-right-detail">
                        <div className="description-detail"> 
                            {this.props.productProperties.descrizione}  
                        </div>  
                        <div className="quantity-select"> 
                          <input type="number" id="quantitySelect"  min="1" max="1000"></input>
                        </div>
                        <div className="riquadro-specifiche">
                          <u>Codice:</u> {this.props.productProperties.codice} <br/>
                          <u>Categoria:</u>  {this.props.productProperties.categorie} <br/>
                          <u>Brand:</u>  {this.props.productProperties.brand}<br/>
                          <u>Dimensioni:</u>   {this.props.productProperties.dimensioni}<br/>      
                          <u>Peso:</u> {this.props.productProperties.peso} <br/>                          
                        </div>
                        <input type="button" value={buttonLabel} className="add-to-cart-button" 
                        description-detail          onMouseOver={this.showPayment} onClick={this.clickCart}/>
                        <span  style={{display:"inline-block"}}>
                          <div className="prezzo-detail">
                              {this.props.productProperties.prezzo}
                              <div className="euro-symbol-detail">€</div>
                          </div>
                        </span>
                      </div>

                    </div>
                </div>             
              

               
               
              
            </div>
  );
}
}
export default ProductDetail;

