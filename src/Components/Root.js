import React from 'react';
import '../CSS/index.css';
import Homepage from './Homepage';



class Root extends React.Component {
  constructor(props){
    super(props);
    this.changeWindow=this.changeWindow.bind(this);
    this.state ={visibleWindowRoot : "Homepage"};
  }


  changeWindow(w){
    this.setState({visibleWindowRoot : w});
    console.log("visibleWindow",this.state.visibleWindowRoot)
  }

  render(){
  return(<div>
          <Homepage openWindow={this.changeWindow} visibleWindow={this.state.visibleWindowRoot}/>
        </div>);
  }
}
export default Root;
