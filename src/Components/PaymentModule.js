import '../CSS/PaymentModule.css';
import React from 'react';

class PaymentModule extends React.Component {
  
  constructor(props){
    super(props);  
    this.changePaymentOption=this.changePaymentOption.bind(this);
    this.setInfo=this.setInfo.bind(this);

    this.state={
        nome:"",
        cognome:"",
        email:"",
        cellulare:"",
        quantita:"",
        comune:"",
        provincia:"",
        cap:"",
        indirizzo:"",
        note:"",
        spedizione:"Corriere"
    }
  }



  changePaymentOption(e){
      var paymentModeString = e.target.id.split(":")[0];
      this.props.changePaymentSelected(paymentModeString);
      
  }




  setInfo(e){
    var namePropertyToSet =e.target.id==="shipping-1-radio" || e.target.id==="shipping-2-radio" || e.target.id==="shipping-3-radio"?
                            "spedizione" : 
                            e.target.id;
    var self=this;
    var newState = Object.assign(self.state);
    newState[namePropertyToSet] = e.target.value;   
 
    console.log("STATO INFO: ", self.state);
  }


  render(){

  return (
              <div className="payment-module">
                    <div className="titolo-payment-form">Modulo di pagamento</div>
                    
                    <div className="total-form">
                      <div className="form-payment">
                        <div className="label-payment">Nome</div><input type="text" id="nome" className="input-payment" defaultValue={this.state.nome} onBlur={this.setInfo}/>
                      </div>
                      <div className="form-payment">
                      <div className="label-payment">Cognome</div><input type="text" id="cognome" className="input-payment" defaultValue={this.state.cognome} onBlur={this.setInfo}/>
                      </div>

                      <div className="form-payment">
                        <div className="label-payment">Email</div><input type="text" id="email" className="input-payment" defaultValue={this.state.email} onBlur={this.setInfo}/>
                      </div>

                      <div className="form-payment">
                        <div className="label-payment">Cell.</div><input type="text" id="cellulare" className="input-payment" defaultValue={this.state.cognome} onBlur={this.setInfo}/>
                      </div>

                      <div className="form-payment">
                        <div className="label-payment">Comune</div><input type="text" id="comune" className="input-payment" defaultValue={this.state.cognome} onBlur={this.setInfo}/>
                      </div>
                      <div className="form-payment">
                        <div className="label-payment">Provincia</div><input type="text" id="provincia" className="input-payment" defaultValue={this.state.cognome} onBlur={this.setInfo}/>
                      </div>
                      <div className="form-payment">
                        <div className="label-payment">Cap.</div><input type="text" id="cap" className="input-payment" defaultValue={this.state.cognome} onBlur={this.setInfo}/>
                      </div>
                      <div className="form-payment">
                        <div className="label-payment">Indirizzo</div><input type="text" id="indirizzo" className="input-payment" defaultValue={this.state.cognome} onBlur={this.setInfo}/>
                      </div>                   

                      <div className="form-payment-note">
                        <div className="label-payment">Note</div><textarea className="input-payment-textarea" id="note" defaultValue={this.state.cognome} onBlur={this.setInfo}/>
                      </div>

                      <div className="form-payment-shippingMode">
                        <div className="label-shipping-mode">Spedizione</div>
                        <label for="shipping-1-radio" className="label-radio-shipping" >Corriere</label>
                        <input id="shipping-1-radio" value="Corriere" defaultChecked  type="radio" name="shipping-radio" checked={this.state.spedizione === "Corriere"} onChange={this.setInfo}/>
                        <label  for="shipping-2-radio" className="label-radio-shipping">Ritiro in negozio</label>
                        <input   id="shipping-2-radio"  value="Ritiro in negozio" type="radio"  name="shipping-radio" checked={this.state.spedizione === "Ritiro in negozio"} onChange={this.setInfo}/>
                        <label  for="shipping-3-radio" className="label-radio-shipping">Altro</label>
                        <input   id="shipping-3-radio" value="Altro" type="radio" name="shipping-radio" checked={this.state.spedizione === "Altro"} onChange={this.setInfo}/>
                      </div>
                      <div className="form-payment-paymentMode">
                        <div>Paga con</div>
                        <label for="PayPal:Radio" className="label-radio-payment">Paypal</label>
                        <input defaultChecked id="PayPal:Radio" type="radio" name="pay-with-radio" onChange={this.changePaymentOption}/>
                        <label for="Postepay:Radio" className="label-radio-payment">Postepay</label>
                        <input disabled id="Postepay:Radio" type="radio"  name="pay-with-radio" onChange={this.changePaymentOption}/>
                        <label for="SumUp:Radio" className="label-radio-payment">SumUp</label>
                        <input disabled id="SumUp:Radio" type="radio" name="pay-with-radio" onChange={this.changePaymentOption}/>
                      </div>
                    </div>
              </div>
  );
}
}
export default PaymentModule;

