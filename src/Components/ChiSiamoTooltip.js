import '../CSS/ChiSiamo.css';
import ChiSiamoProperties from '../Properties/ChiSiamoProperties'
import React from 'react';

class ChiSiamoTooltip extends React.Component {


  render(){
    var biographyTop =  ChiSiamoProperties.biographyTop;
    var biographyBottom = ChiSiamoProperties.biographyBottom;
        

  return (
            <div className="chi-siamo-tooltip-container" onMouseOver={this.props.openParentMethod} onMouseLeave={this.props.closeParentMethod}>
              <div className="title-chi-siamo">
                  Chi siamo  
              </div> 
            
              <div className="foto-famiglia-laterale">
                
              </div><div className="biography-top">
                  {biographyTop}  
              </div>
              <div className="biography-bottom">
                  {biographyBottom}  
              </div>
              <div className="foto-famiglia-sotto">
              
              </div>
              <div className="bottom-space-chi-siamo">

              </div>
            </div>
  );
}
}
export default ChiSiamoTooltip;

