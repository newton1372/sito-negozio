import '../CSS/Partners.css';
import PartnersList from '../Properties/PartnersList';
import React from 'react';




class PartnersTooltip extends React.Component {
  constructor(props){
    super(props);  
    this.togglePartnerCategory = this.togglePartnerCategory.bind(this);

    this.state={
      categoryPartnerSelected:""     
     } 
    
  }




  togglePartnerCategory(e){
    var self=this;
    var newState = Object.assign(self.state);
    newState.categoryPartnerSelected = this.state.categoryPartnerSelected !== e.target.innerHTML ? 
                                     e.target.innerHTML : "";
    self.setState(newState);

  }



  render(){
    var self=this;      

  return (
              <div className="partners-tooltip-container" onMouseOver={this.props.openParentMethod} onMouseLeave={this.props.closeParentMethod}>
                <div className="titolo-partners">
                  Eventi da favola
                </div>
                <div className="subtitle-partners">
                  Prenota da qui il tuo evento e avrai il 10% di sconto sui tuoi acquisti qui su Scifo Regali!
                </div>

                <div className="partners-list"/>
                    {
                      PartnersList.partnersCategories.map(function(category){
                        var partnersArrayOfCategory = PartnersList.partners.filter(function(partner){
                                                            return partner.category === category;
                                                          });

                        var isVisible = self.state.categoryPartnerSelected !==category?
                                                  "invisible-block":"";
                        return(
                          <div>
                            <button className="partners-category-div" onClick={self.togglePartnerCategory}>
                                {category}
                            </button>                          
                            <div className= {isVisible} >
                              {
                                partnersArrayOfCategory.map(function(item){
                                
                                  return (
                                    <div className ="partner-block">
                                        <div className="name-partner">{item.nome}</div>
                                        <div className="field-partner">{item.indirizzo}</div>
                                        <div className="field-partner">{item.cell}</div>
                                        <div className="field-partner">{item.email}</div>
                                        <div className="field-partner">{item.category}</div>
                                        <div className="field-partner"><a href={item.link}>{item.link}</a></div>
                                     </div>
                                  );

                                })
                              }
                            </div>
                          </div>
                          )
                        })
                    }              
                </div>
                
  );
}
}
export default PartnersTooltip;

