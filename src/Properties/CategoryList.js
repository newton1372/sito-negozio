const CategoryList = {
  listOfLeftCategories: [
    "Lista nozze",
    "Idee regalo",
    "Bomboniere",
    "Battesimi",
    "Matrimoni",
    "Prima comunione",
    "Promozioni"
  ],
  listOfRightCategories: [
    "Dille che la ami",
    "Bigiotteria",
    "Cresime",
    "Orologi",
    "Quadri",
    "Capezzali",
    "Franchising"
  ]
}

export default CategoryList