const PartnersList = {
  partnersCategories: ["Fiorai", "DJ", "Sale ricevimenti", "Animazione", "Ristoranti"],
  partners:[
    {
      nome: "Il Fiorario",
      indirizzo: "Via S. Martini n.2 Rosolini (SR)",
      cell: "0931404040",
      email: "fioraioAzzeccagarbugli@gmail.it",
      category: "Fiorai",
      link: "www.iosonoilfioraio.it"
    },
    {
      nome: "Mille e un fiore",
      indirizzo: "Via S. Gennaro n.40 Modica (RG)",
      cell: "0931506931",
      email: "fioraio&Mille@gmail.it",
      category: "Fiorai",
      link: "www.iosonoilsecondofioraio.it"
    },
    {
      nome: "Antica Macina",
      indirizzo: "SS 115 Rosolini(SR)",
      cell: "3392149877",
      email: "Antica.Macina@gmail.it",
      category: "Ristoranti",
      link: "www.iosonoilristorante.org"
    },
    {
      nome: "L'Oasi dei Re",
      indirizzo: "SS 115 Rosolini(SR)",
      cell: "3392149877",
      email: "fioraio&Mille@gmail.it",
      category: "Sale ricevimenti",
      link: "www.oasideire.com"

    },
    {
      nome: "Mastro Fabrizio Mr Truzzo",
      indirizzo: "Via Asiago 21 Rosolini(SR)",
      cell: "3344141231",
      email: "fioraio&Mille@gmail.it",
      category: "DJ ricevimenti",
      link:undefined
    },
  ]
}

export default PartnersList